output "ip_address" {
    value = "${google_compute_instance.vm_instance.*.network_interface.0.access_config.0.nat_ip}"
}

output "list_hostname" {
  value = "${google_compute_instance.vm_instance[*].name}"
}

data "google_project" "project" {
}

output "project_id" {
  value = "${google_compute_instance.vm_instance[0].project}"
}